import sys
import lxml.etree as ET
import array as arr
import re
import os
import glob
import re
#arguments par défault
output = 'single'
cycle = ''


#fichier où on souhaite écrire les coordonnées metapost
folder = 'svg/'
#eventuel coefficient pour diviser/mulitplier les coordonnées obtenues

coeff = 1;
#definir pen
pen = ''

char = sys.argv[1]
if len(sys.argv) > 2:
	print('lol')
	ascii = sys.argv[2]
else:
	ascii = ord(char)
	
file = "svg/" + char + ".svg"

#fonction qui va écrire les coordonnées metapost dans le fichier file

def operation(coor):
	coors = coor.split(',')
	coorX = round( (float(coors[0]) -15), 3)
	coorY = - round( (float(coors[1]) - 23.322), 3)
	
	return str(coorX) + "ux, "+ str(coorY) + "uy"



def buildMp(lettre):
	
	
	newdraw = ""
	
	tree = ET.parse(lettre)
	print(tree)
	root = tree.getroot()
	paths = root.findall(".//{http://www.w3.org/2000/svg}path")
	print(paths)
	coord = ""
	coorXName = []
	coorYName = []
	coorX =  []
	coorY = []
	i = 1
	for path in paths:
		
		dessin = path.get('d')
		
		
		
		dessins = dessin.split(' ')
		
		#print(dessins)
		
		for index, d in enumerate(dessins):
			print(d)
			#print("i", index)
			if (d == "M"):
	
				newdraw += "draw ("
			elif (d == "C"):
				coor1 = dessins[index +1]
				coor2 = dessins[index +2]
				coor3 = dessins[index +3]
				#print(coor1)
				#print(coor2)
				#print(coor3)
				
				
				coorXName.append("x" + str(i) + "a")
				coorX.append(float(coor1.split(',')[0]))
				

				
				coorYName.append("y" + str(i) + "a")
				coorY.append(float(coor1.split(',')[1]))
				
		
				
				coorXName.append("x" + str(i) + "b")
				coorX.append(float(coor2.split(',')[0]))
				coorYName.append("y" + str(i) + "b")
				coorY.append(float(coor2.split(',')[1]))
				
				newdraw += "controls (z" + str(i) + "a) and (z" + str(i) + "b).."
				
				i = i + 1
				
		
				
				coorXName.append("x" + str(i))
				coorX.append(float(coor3.split(',')[0]))
				coorYName.append("y" + str(i))
				coorY.append(float(coor3.split(',')[1]))
				
				newdraw += "z" + str(i) + ".."
				
				i = i + 1
				
				dessins.remove(coor1)
				dessins.remove(coor2)
				dessins.remove(coor3)
			
			elif (d == "V"):
				
				coorAft = dessins[index +1]
				coorBefX = coorX[-1]
				#coorBefY = coorY[-1]
				print(coorAft)
				#print(coorAft)
				#print('coor1' + coorX)
				
				
				
				print('bug here')
				coorXName.append("x" + str(i))
				try:
					coorX.append(coorBefX)
			
				except:
					coorX.append(0)
					
				coorYName.append("y" + str(i))
				coorY.append(float(coorAft))
				
				dessins.remove(coorAft)
				
				newdraw += "z" + str(i) + ".."
				
				i = i + 1
			
			elif (d == "H"):
				#coorBefX = coorX[-1]
				coorBefY = coorY[-1]
				coorAft = dessins[index +1]
				
				
				#print('coor1' + coorX)
				
				
				
				
				coorXName.append("x" + str(i))
				coorX.append(float(coorAft))
				coorYName.append("y" + str(i))
				coorY.append(coorBefY)
				
				dessins.remove(coorAft)
				
				newdraw += "z" + str(i) + ".."
				
				i = i + 1
			elif (d == "L"):
				
				print('')
			elif (d == "Z"):
				print('')
			else:
				print('')
				
				coorXName.append("x" + str(i))
				coorX.append(float(d.split(",")[0]))
				coorYName.append("y" + str(i))
				coorY.append(float(d.split(",")[1]))
				newdraw += "z" + str(i) + ".."
				i = i + 1
				#newdraw += "("+ operation(d) + ")..";
		
		#print(min(coorX))
		#print(min(coorY))
	coorYpreclean =[]
	coorYclean =[]
	for index, d in enumerate(coorYName):
	
		if "a" in d:
			print('')
		elif "b" in d:
			print('')
		else:
			coorYpreclean.append(index)	
	for d in coorYpreclean:
		coorYclean.append(coorY[d])
	#print(coorYclean) 
	
	for index, d in enumerate(coorXName):
		coord += d.replace("x","z") + ":= (" + str( round(coorX[index] - min(coorX), 4)) + "ux, " + str(round(- coorY[index] + max(coorY), 4)) + "uy);\n"
		
	#for index, d in enumerate(coorYName):
		#coord += d + ":= " + str(round(- coorY[index] + max(coorY), 4)) + "uy;\n"
			
		
	newdraw = newdraw + ";"
	newdraw = newdraw.replace("..;", ") slanted slant;")
	newdraw = newdraw.replace("..draw", "; \n draw")
	newdraw = newdraw + "\nendchar(10);"
	newdraw = "%" + char + "\ninput ../def;\n beginchar("+ str(ascii) +",10);\n" + coord + newdraw;
		
	f = open( "mp/" + str(ascii) +".mp", "w")
	f.write(newdraw)
	print(newdraw)
		
		
			
			
with open(file, 'r') as lettre:
	buildMp(lettre)

