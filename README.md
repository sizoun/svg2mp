# SVG2MP

svg2mp est un script python permettant de convertir un svg en metapost, pour l'utiliser dans [Plancton](https://gitlab.com/Luuse/plancton/plancton-editor)

## Dépendances
- Python3

## Arborescence

```
├── svg2mp // script python
│
├── svg
│   ├── a.svg
│   ├── b.svg
│   └── c.svg
│
├── mp
    ├── ... // résultat de la conversion
    └── ...
```

## Utilisation

- Glisser chaque dessin de lettre svg dans le dossier svg avec la nomenclature <glyphe>.svg (a.svg, b.svg, c.svg...). Attention, les path du SVG doivent être au format ABSOLU et les commandes répétitives. (Export depuis Inkscape : Préférences > Sortie SVG > Données de chemin)

- Dans le terminal : 

```
cd svg2mp
python3 svg2mp.py <glyphe à convertir> // par exemple, python3 svg2mp.py a
```

